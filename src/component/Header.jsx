import React from "react";
export default function ({title,bg=null}) {
    return (
        <div className={"header all-center"} style={{backgroundImage:`url('${bg?bg:"https://myclouddoor.com/wp-content/uploads/2016/07/tech-background-image-1-1.jpg"}')`}} >
            <h1>{title}</h1>
        </div>
    )
}