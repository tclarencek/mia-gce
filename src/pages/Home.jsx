import React from 'react'
import {
  Icon,
  Table,
  Container,
  Form,
  Button,
  Progress,
  Loader,
  Dimmer
} from "semantic-ui-react";
import Header from "../component/Header";
import { withRouter } from "react-router-dom";
 class Home extends React.Component{
    state={classes:[],loading:true,addName:"",addFile:null,percent:0,uploading:false};

    saveData = ()=>{
        let {addName,addFile} = this.state;
        if (!addName) {
            alert("Name required")
            return
        }
        if(addFile){
           let  file_name = addFile.name.split(".").join("."+Math.random()+".")
            this.save(addFile,file_name,addName); 
        }else{
          this.save(null,null,addName);   
        }
       
    }

    save = (file,fileName,documentName)=>{
        let ctx=this
        ctx.setState({loading:true})
        let firebase = window.fb
        // Upload file and metadata to the object 'images/mountains.jpg'
        if(file){
            ctx.setState({uploading:true})
            var uploadTask = window.storageRef
              .child("images/" + fileName)
              .put(file);

            uploadTask.on(
              firebase.storage.TaskEvent.STATE_CHANGED,
              function(snapshot) {
                var progress =
                  (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
                ctx.setState({percent:progress})
                switch (snapshot.state) {
                  case firebase.storage.TaskState.PAUSED:
                    console.log("Upload is paused");
                    break;
                  case firebase.storage.TaskState.RUNNING:
                    console.log("Upload is running");
                    break;
                }
              },
              function(error) {
                  console.log(error)
                   ctx.setState({ loading: false,uploading:false });
                alert("Err " + error.code);
              },
              function() {
                  ctx.setState({ uploading: false });
                uploadTask.snapshot.ref
                  .getDownloadURL()
                  .then(function(downloadURL) {
                    console.log("File available at", downloadURL);
                    ctx.completeSave("classes", {
                      name: documentName,
                      image: downloadURL
                    });
                  });
              }
            );
        }else{
            this.completeSave("classes", {
              name: documentName,
              image: ""
            });
        }
    }

    completeSave=(collection,data)=>{
        let ctx=this;
        window.database.collection(collection)
          .add(data)
          .then(function(docRef) {
        
              
               ctx.setState({ loading: false,addName:"",addFile:null },()=>{
                   ctx.getClasses();
                   document.getElementById("thefile").value=null;
               });
              
            console.log("Document written with ID: ", docRef.id);
          })
          .catch(function(error) {
              ctx.setState({ loading: false });
              alert("Error saving");
            console.error("Error adding document: ", error);
          });
    }

    getClasses=()=>{
         let context = this;
         context.setState({loading:true})
         window.database
           .collection("classes")
           .get()
           .then(querySnapshot => {
             let classes = [];
             querySnapshot.forEach(doc => {
               classes.push(doc);
               //console.log(`${doc.id} => ${doc.data()}`);
             });
             let loading = false;
             context.setState({ classes, loading });
           })
           .catch(e => {
             let loading = false;
             context.setState({ loading });
             alert("Error ");
             console.error(e);
           });
    }
    componentDidMount(){
       this.getClasses()
    }
    render(){
        let tableContent = this.state.classes.map(zclass=>{
            let classData = zclass.data()
            return (
              <Table.Row>
                <Table.Cell collapsing>
                  <Icon name="group" /> {zclass.id}
                </Table.Cell>
                <Table.Cell>{classData.name}</Table.Cell>
                <Table.Cell>
                  <a href={classData.image}>{classData.image}</a>
                </Table.Cell>
                <Table.Cell collapsing textAlign="right">
                  <a style={{ cursor: "pointer" }} onClick={e => {
                      this.props.history.push("/class/"+zclass.id)
                  }}>
                    <Icon name="eye" /> View
                  </a>
                </Table.Cell>
                <Table.Cell collapsing textAlign="right">
                  <a 
                    style={{ cursor: "pointer" }}
                    onClick={elt => {
                      let context = this;
                      context.setState({ loading: true });
                      window.database
                        .collection("classes")
                        .doc(zclass.id)
                        .delete()
                        .then(e => {
                          context.setState({ loading: false });
                          context.getClasses();
                        })
                        .catch(e => {
                          context.setState({ loading: false });
                          alert("Error deleting");
                        });
                      //   var query = window.database
                      //     .collection("classes")
                      //     .where("id", "==", zclass.id);
                      //   query
                      //     .get()
                      //     .then(function(querySnapshot) {
                      //       querySnapshot.forEach(function(doc) {
                      //         doc.ref.delete();
                      //       });
                      //       context.setState({ loading: false });
                      //       context.getClasses();
                      //     })
                      //     .catch(e => {
                      //       context.setState({ loading: false });
                      //       alert("Error deleting");
                      //     });
                    }}
                  >
                    <Icon name="trash" /> Delete
                  </a>
                </Table.Cell>
              </Table.Row>
            );
        })
        return (
          <div>
            <Header title={"Classes"} />
            <Container>
              {this.state.loading && (
                <Dimmer active inverted>
                  <Loader inverted>Loading</Loader>
                </Dimmer>
              )}
              <br />
              <h3>Information about Classes</h3>
              <hr style={{ border: "1px solid #eee" }} />
              <br />
              <br />
              <Form loading={false}>
                <Form.Field>
                  <label>Class Name</label>
                  <input
                    placeholder="Class Name"
                    value={this.state.addName}
                    onChange={e => this.setState({ addName: e.target.value })}
                  />
                </Form.Field>
                <Form.Input
                  label="Image"
                  type="file"
                  id="thefile"
                  accept={"image/*"}
                  onChange={e => this.setState({ addFile: e.target.files[0] })}
                />
                {this.state.uploading && (
                  <Progress percent={this.state.percent} indicating />
                )}
                <Button
                  type="submit"
                  onClick={this.saveData}
                  disabled={this.state.loading || this.state.uploading}
                >
                  Submit
                </Button>
              </Form>
              <br />
              <TableElement title="Classes" content={tableContent} />
            </Container>
          </div>
        );
    }
}

const TableElement = ({title,content}) => (

        <Table celled striped >
            <Table.Header>
                <Table.Row>
                    <Table.HeaderCell colSpan='5'>{title}</Table.HeaderCell>
                </Table.Row>
            </Table.Header>

            <Table.Body>
                {content}
            </Table.Body>
        </Table>

)
export default withRouter(Home)