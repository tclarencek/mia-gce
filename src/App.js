import React from 'react';
import './App.css';
import Home from './pages/Home';
import Subjects from './pages/Subjects';
import Chapters from "./pages/Chapters";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom"
function App() {
  
  return (
    <Router>
      <Switch>
        <Route path="/" component={Home} exact />
        <Route path="/class/:id" component={Subjects} exact />
        <Route path="/subject/:cid/:sid" component={Chapters} exact />
        <Route>
          <div className="all-center" style={{ height: "100%", width: "100%" }}>
            <br />
            <br />
            <br />
            <br />
            <h1 style={{ textAlign: "center" }}>404 Ressource Not Found!</h1>
          </div>
        </Route>
      </Switch>
    </Router>
  );
}

export default App;
